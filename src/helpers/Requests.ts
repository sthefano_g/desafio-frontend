import axios from "axios"
import { dateConversion } from "../utils/dateConversion"

const gitHub = axios.create({
  baseURL: "https://api.github.com/",
  method: "GET",
  headers: {
    Accept: "application/vnd.GitHub.v3+json",
    Authorization: "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",
  },
})

export async function getFullUserInfo(username: string) {
  try {
    const res = await gitHub.get("search/users", { params: { q: username } })
    const searchRes = res.data
    return Promise.all(
      searchRes.items.map(async (user: any) => {
        const userData = await getUserData(user.login)
        return {
          login: user.login,
          id: user.id,
          avatar_url: user.avatar_url,
          url: user.html_url,
          score: user.score,
          ...userData,
        }
      })
    )
  } catch (e) {
    console.log("Erro na requisição. Tente novamente")
  }
}

export async function getUserData(username: string) {
  try {
    const res = await gitHub.get(`users/${username}`)
    const userRes = res.data
    return {
      name: userRes.name,
      followers: userRes.followers,
      following: userRes.following,
      created: dateConversion(userRes.created_at),
    }
  } catch (e) {
    console.log("Erro na requisição. Tente novamente")
    return
  }
}
