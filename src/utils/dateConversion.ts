export function dateConversion(date: string) {
  const formatDate = date.substring(10, -1)
  return formatDate.split("-").reverse().join("/")
}
