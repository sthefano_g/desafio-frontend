const size = {
  mobile: "720px",
  tablet: "768px",
  desktop: "1200px",
}

export const breakpoints = {
  mobile: `(max-width: ${size.mobile})`,
  tablet: `(max-width: ${size.tablet})`,
  desktop: `(max-width: ${size.desktop})`,
}
