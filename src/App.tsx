import { AppContainer } from "./components/styled/AppContainer"
import Home from "./pages/Home"
import Header from "./components/Header"
import { Footer } from "./components/Footer"
import Results from "./components/Results"
import React from "react"
import * as api from "./helpers/Requests"
import { ContentContainer } from "./components/styled/AppStyles"

function App() {
  const [hasSearch, setHasSearch] = React.useState(false)
  const [apiResult, setApiResult] = React.useState([])
  const [searchInput, setSearchInput] = React.useState("")

  const requestData = async (username: string) => {
    if (username) {
      let data: any = await api.getFullUserInfo(username)
      setHasSearch(true)
      setApiResult(data)
      return data
    }
  }
  return (
    <AppContainer>
      <Header
        onSearchSubmit={requestData}
        searchInput={searchInput}
        setSearchInput={setSearchInput}
      />
      <ContentContainer>
        {hasSearch ? (
          <Results users={apiResult} title={searchInput} />
        ) : (
          <Home />
        )}
      </ContentContainer>
      <Footer txt='Projetado por: Sthéfano Garcia - 16/03/2022' />
    </AppContainer>
  )
}

export default App
