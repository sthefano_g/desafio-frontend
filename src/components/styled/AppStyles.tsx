import styled from "styled-components"
import { breakpoints } from "../../utils/breakpoints"

export const StyledContainer = styled.div`
  display: flex;
  margin: auto;
  max-width: 980px;
`

export const Logo = styled.div`
  margin-right: 20px;
  @media ${breakpoints.tablet} {
    align-self: center;
    margin-bottom: 15px;
  }

  @media ${breakpoints.mobile} {
    height: 20px;
    margin-right: 0;
  }

  @media ${breakpoints.desktop} {
    height: 36px;
  }
  img {
    max-height: 36px;
  }
  a {
    text-decoration: none;
  }
`

export const NotFound = styled.div`
  display: flex;
  font-size: 25px;
  font-weight: 600;
  padding: 20px;
  align-items: flex-start;
`

export const StyledHeader = styled.div`
  box-sizing: border-box;
  display: flex;
  max-width: 100vw;
  padding: 10px 40px;
  background: #ffffff;
  @media ${breakpoints.mobile} {
    flex-direction: column;
  }
  @media ${breakpoints.tablet} {
  }
`

export const StyledSearchBox = styled.div`
  display: flex;
  width: 100%;
  max-height: 36px;
  input {
    width: 100%;
    border: 1px solid #e1dfe0;
    border-radius: 4px 0px 0px 4px;
    padding: 15px 10px;
    outline: none;
  }
  @media ${breakpoints.mobile} {
    margin-bottom: 15px;
  }
`

export const StyledSearchButton = styled.div`
  button {
    max-height: 100%;
    padding: 8px;
    background: #8c56c2;
    box-shadow: -2px 0px 4px rgba(0, 0, 0, 0.25);
    border-radius: 0px 4px 4px 0px;
    border: none;
    &:hover {
      cursor: pointer;
      background: #ad86d3;
      transition: ease-out 100ms;
    }
    &:active {
      box-shadow: 0 5px #666;
      transform: translateY(1px);
      animation: ease;
    }
  }
`

export const StyledHome = styled.div`
  background-image: url("./resources/homeBg.svg");
  min-height: inherit;
  background-position: center;
  background-repeat: no-repeat;
  @media ${breakpoints.mobile} {
    background-size: 95%;
  }
`

export const StyledResults = styled.div`
  min-height: calc(100vh - 93px);
  background: linear-gradient(
      rgba(255, 255, 255, 0.7),
      rgba(255, 255, 255, 0.7)
    ),
    url("./resources/resultsBg.svg");
  background-position: center;
  background-size: 100%;
  padding-bottom: 20px;
`

export const ResultContainer = styled.div`
  margin-top: 20px;
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
  width: 100%;
  justify-content: center;
`

export const StyledLoading = styled.div`
  width: 100%;
  position: absolute;
  font-size: 100px;
  text-align: center;
  font-weight: 200;
`

export const StyledFooter = styled.div`
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  max-height: 32px;
  align-self: flex-end;
  padding: 10px;
  background: #ffffff;
  box-shadow: 0px -4px 4px rgba(47, 37, 68, 0.15);
  text-align: center;
  font-size: 14px;
  color: #7b6490;
`

export const ResultTitle = styled.div`
  width: 100%;
  min-height: 30px;
  border-bottom: 1px solid #e1dfe0;
  font-size: 18px;
  line-height: 21px;
  font-weight: 400;
  padding: 10px 10px 0px 10px;
  @media ${breakpoints.mobile} {
    text-align: center;
  }
  @media ${breakpoints.tablet} {
    text-align: center;
  }
  @media ${breakpoints.desktop} {
    text-align: center;
  }
`
export const CardContainer = styled.div`
  padding-bottom: 20px;
  margin-bottom: 20px;
  background-color: #ffffff;
  border-radius: 10px;
  box-shadow: 0px 0px 6px rgba(47, 37, 68, 0.25);
  max-width: 25%;
  min-width: 266px;
`

export const Avatar = styled.div`
  min-height: 130px;
  border-radius: 10px 10px 0px 0px;
  background-position: center center;
  background-size: cover;
`

export const UserInfo = styled.div`
  padding: 10px;
  display: flex;
  min-height: 100px;
  width: 230px;
  word-break: break-word;
  flex-direction: column;
  p,
  a {
    margin-bottom: 10px;
  }
`

export const NameText = styled.div`
  margin: 0px;
  font-size: 18px;
  color: #2f2544;
  line-height: 21px;
  display: flex;
`
export const Link = styled.a`
  padding: 0;
  margin: 0;
  font-size: 12px;
  color: #03cada;
  width: 100%;
  font-weight: 400;
  border: none;
  line-height: 14px;
  display: flex;
  margin-top: 10px;
  &:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`

export const ScoreText = styled.div`
  padding-top: 4px;
  margin: 0px;
  font-size: 12px;
  color: #7b6490;
  height: 14px;
  display: flex;
`
export const ContentContainer = styled.div`
  max-width: 100vw;
  min-height: calc(100vh - 93px);
`

export const OpenModalBtn = styled.button`
  color: #ffffff;
  height: 36px;
  width: 90%;
  background-color: #8c56c2;
  border: none;
  border-color: #8c56c2;
  border-radius: 20px;
  font-size: 14px;
  text-transform: uppercase;
  &:hover {
    background-color: #ad86d3;
    cursor: pointer;
    transition: ease-out 100ms;
  }
  @media ${breakpoints.mobile} {
    width: 90%;
    font-size: 12px;
  }
`
export const ButtonContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`

export const ModalContainer = styled.div`
  box-sizing: border-box;
  display: flex;
  width: 980px;
  padding: 20px;
  @media ${breakpoints.tablet} {
    flex-direction: column;
  }
`

export const ModalUserInfo = styled.div`
  display: flex;
  width: 90%;
  flex-direction: column;
  padding: 10px 0px 0px 20px;
  a {
    text-decoration: none;
    color: inherit;
  }
  a:hover {
    text-decoration: underline;
  }
  @media ${breakpoints.mobile} {
    min-width: 100%;
    overflow: hidden;
  }
  @media ${breakpoints.tablet} {
    overflow: hidden;
    padding: 0;
  }
  @media ${breakpoints.desktop} {
    max-width: 45vw;
    overflow: hidden;
  }
`

export const UserProps = styled.div`
  font-size: 14px;
  line-height: 16px;
  color: #2f2544;
  margin-bottom: 20px;
`
export const UserProps_Right = styled.div`
  font-size: 14px;
  line-height: 16px;
  text-align: right;
  color: #2f2544;
  margin-bottom: 20px;
  @media ${breakpoints.tablet}{
    text-align: left;
  }
`
export const UserText = styled.div`
  font-size: 14px;
  line-height: 16px;
  color: #7b6490;
  margin-top: 5px;
`

export const ModalName = styled.div`
  font-size: 18px;
  line-height: 21px;
  display: flex;
  color: #2f2544;
  border-bottom: 1px solid #e1dfe0;
  width: 100%;
  @media ${breakpoints.tablet} {
    justify-content: center;
    margin-top: 20px;
  }
`
export const CloseModalBtn = styled.button`
  border-radius: 3px;
  background: #ffffff;
  color: #8c56c2;
  border: 1px solid #8c56c2;
  height: 36px;
  text-align: center;
  font-weight: 600;
  text-transform: uppercase;
  &:hover {
    color: #ffffff;
    background: #8c56c2;
    transition: ease-out 100ms;
    cursor: pointer;
  }
  @media ${breakpoints.tablet}
  {
    width: 100%;
  }
`
export const ModalAvatar = styled.div`
  display: flex;
  min-width: 266px;
  min-height: 266px;
  border-radius: 10px;
  background-position: center center;
  background-size: cover;
`

export const ModalBtnContainer = styled.div`
  display: flex;
  height: 100%;
  align-self: flex-end;
  align-items: flex-end;
  @media ${breakpoints.tablet} {
    align-self: center;
    width: 100%;
  }
`

export const ColumnModalInfo = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 15px;
  @media ${breakpoints.tablet}{
    flex-direction: column;
    align-items: flex-start;
  }
`
export const SimpleDiv = styled.div``
