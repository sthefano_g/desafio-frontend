import React from "react"
import { StyledFooter } from "./styled/AppStyles"

interface FooterTypes {
  className?: string
  txt: string
}

export function Footer({ txt }: FooterTypes) {
  return (
    <footer>
      <StyledFooter>{txt} </StyledFooter>
    </footer>
  )
}
