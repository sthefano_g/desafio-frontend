import Modals from "./Modals"
import * as S from "./styled/AppStyles"

export interface CardTypes {
  avatar_url: string
  url: string
  name: string
  score?: string
  login: string
  created: string
  followers: number
  following: number
  toggleModal?: () => void
}

export function Card(user: CardTypes) {
  const modalInfo = {
    name: user.name,
    url: user.url,
    avatar_url: user.avatar_url,
    login: user.login,
    created: user.created,
    followers: user.followers,
    following: user.following,
  }
  return (
    <S.CardContainer>
      <S.SimpleDiv>
      <S.Avatar style={{ backgroundImage: `url(${user.avatar_url})` }} />
      <S.UserInfo>
        <S.NameText>{user.name != null ? user.name : user.login}</S.NameText>
        <S.Link onClick={() => window.open(user.url, "_blank")}>
          {user.url}
        </S.Link>
        <S.ScoreText>Score: {user.score}</S.ScoreText>
      </S.UserInfo>
      <S.ButtonContainer>
        <Modals data={modalInfo} />
      </S.ButtonContainer>
      </S.SimpleDiv>
    </S.CardContainer>
  )
}
