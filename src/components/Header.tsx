import React from "react"
import * as S from "./styled/AppStyles"
import { SearchBox, HandleChange } from "./SearchBox"

export default function Header(props: {
  onSearchSubmit: (username: string) => void, searchInput: string, setSearchInput: (value: string) => void
}) {

  const handleChange: HandleChange = (e) => {
    props.setSearchInput(e.target.value)
  }

  return (
    <S.StyledHeader>
      <S.Logo>
        <a href='/'>
          <img src='./resources/logo.svg' alt='The Git Search'></img>
        </a>
      </S.Logo>
      <SearchBox
        placeholder='Pesquisar'
        value={props.searchInput}
        onChange={handleChange}
        onSubmit={props.onSearchSubmit}
      />
    </S.StyledHeader>
  )
}
