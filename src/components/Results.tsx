import {
  NotFound,
  StyledContainer,
  StyledResults,
  ResultContainer,
  ResultTitle,
} from "./styled/AppStyles"
import { Card } from "./Card"
import { useState } from "react"

export interface ResultTypes {
  key: number
  login: string
  id: number
  avatar_url: string
  url: string
  name: string
  followers: number
  following: number
  score: number
  created: string
}

export default function Results(props: {
  users: ResultTypes[]
  title: string
}) {
  const [modalIsOpen, setIsOpen] = useState(false)

  const openModalFc = () => {
    setIsOpen(!modalIsOpen)
    console.log(modalIsOpen)
  }

  if (props.users.length === 0) {
    return (
      <StyledResults>
        <StyledContainer>
          <NotFound>Usuário não encontrado!</NotFound>
        </StyledContainer>
      </StyledResults>
    )
  }
  return (
    <StyledResults>
      <StyledContainer>
        <ResultContainer>
          <ResultTitle>Resultados para: {props.title}</ResultTitle>
          {props.users.map((user, index) => (
            <Card
              key={index}
              avatar_url={user.avatar_url}
              url={user.url}
              name={user.name}
              score={user.score.toFixed(2)}
              login={user.login}
              created={user.created}
              followers={user.followers}
              following={user.following}
              toggleModal={openModalFc}
            />
          ))}
        </ResultContainer>
      </StyledContainer>
    </StyledResults>
  )
}
