import React from "react"
import { StyledSearchBox, StyledSearchButton } from "./styled/AppStyles"

export type HandleChange = (e: React.ChangeEvent<HTMLInputElement>) => void

export function SearchBox(props: {
  placeholder: string
  value: string
  onSubmit: (value: string) => void
  onChange: HandleChange
}) {
  function enterSubmit(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.code === "Enter") {
      props.onSubmit(props.value)
    }
  }
  return (
    <StyledSearchBox>
      <input
        type='text'
        name='searchBox'
        value={props.value}
        placeholder={props.placeholder}
        onChange={props.onChange}
        onKeyDown={enterSubmit}
      ></input>
      <StyledSearchButton>
        <button onClick={() => props.onSubmit(props.value)}>
          <img src='../resources/searchIcon.svg' alt='Pesquisar'></img>
        </button>
      </StyledSearchButton>
    </StyledSearchBox>
  )
}
