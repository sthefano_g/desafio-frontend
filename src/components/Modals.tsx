import React from "react"
import ReactModal from "react-modal"
import { CardTypes } from "./Card"
import {
  CloseModalBtn,
  ModalAvatar,
  ModalBtnContainer,
  ModalContainer,
  ModalUserInfo,
  OpenModalBtn,
  ColumnModalInfo,
  StyledContainer,
  UserProps,
  UserText,
  ModalName,
  SimpleDiv,
  UserProps_Right,
} from "./styled/AppStyles"

export default function Modals(props: { data: CardTypes }) {
  const [isOpen, setOpen] = React.useState(false)

  function handleOpen() {
    setOpen(true)
  }

  function handleClose() {
    setOpen(false)
  }

  const errorText = 'ERRO! Não carregou'

  return (
    <>
      <OpenModalBtn onClick={handleOpen}>Ver Mais</OpenModalBtn>
      <ReactModal
        contentLabel='Detalhes do usuário'
        isOpen={isOpen}
        ariaHideApp={false}
        onRequestClose={handleClose}
        style={{
          content: {
            zIndex: "0",
            maxWidth: "80%",
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            overflowX: "auto",
            borderRadius: "10px",
          },
          overlay: {
            backgroundColor: "rgba(47, 37, 68, 0.2)",
            backdropFilter: "blur(2px)",
          },
        }}
      >
        <StyledContainer>
          <ModalContainer>
            <ModalAvatar
              style={{ backgroundImage: `url('${props.data.avatar_url}')` }}
            />
            <ModalUserInfo>
              <ModalName>
                {props.data.name ? props.data.name : props.data.login}
              </ModalName>
              <ColumnModalInfo>
                <SimpleDiv>
                  <UserProps>
                    Username:
                    <UserText>{props.data.login}</UserText>
                  </UserProps>
                  <UserProps>
                    Cadastrado(a):
                    <UserText> {props.data.created !=null ? props.data.created : `${errorText}`}</UserText>
                  </UserProps>
                  <UserProps>
                    URL:
                    <UserText>
                      <a href={props.data.url}>{props.data.url}</a>
                    </UserText>
                  </UserProps>
                </SimpleDiv>
                <SimpleDiv>
                  <UserProps_Right>
                    Seguindo:
                    <UserText>
                      {props.data.following !=null ? props.data.following : `${errorText}`}
                    </UserText>
                  </UserProps_Right>
                  <UserProps_Right>
                    Seguidores:
                    <UserText>
                      {props.data.followers !=null ? props.data.followers : `${errorText}`}
                    </UserText>
                  </UserProps_Right>
                </SimpleDiv>
              </ColumnModalInfo>
              <ModalBtnContainer>
                <CloseModalBtn onClick={handleClose}> Fechar </CloseModalBtn>
              </ModalBtnContainer>
            </ModalUserInfo>
          </ModalContainer>
        </StyledContainer>
      </ReactModal>
    </>
  )
}
